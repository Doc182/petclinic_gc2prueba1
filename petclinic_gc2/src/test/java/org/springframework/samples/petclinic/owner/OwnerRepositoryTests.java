package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTests {

	@Autowired
	private OwnerRepository owners;
	private Owner george;

	@Before
	public void init() {

		if (this.george == null) {
			george = new Owner();
			george.setFirstName("Geormig");
			george.setLastName("Frankmig");
			george.setAddress("110 W. Libert");
			george.setCity("Madison");
			george.setTelephone("608678900");
		}
		this.owners.save(george);

	}

	@Test
	public void testDeleteById() {
		
		
		
        Owner propietario = this.owners.findById(george.getId());
		
		assertNotNull(propietario.getFirstName());
		assertEquals(propietario.getFirstName(),this.george.getFirstName());

		assertNotNull(propietario.getLastName());
		assertEquals(propietario.getLastName(), this.george.getLastName());

		assertNotNull(propietario.getAddress());
		assertEquals(propietario.getAddress(), this.george.getAddress());

		assertNotNull(propietario.getCity());
		assertEquals(propietario.getCity(), this.george.getCity());

		assertNotNull(propietario.getTelephone());
		assertTrue(propietario.getTelephone() == this.george.getTelephone());

		assertNotNull(this.george.getId());

		this.owners.deleteById(this.george.getId());

		assertNull(this.owners.findById(george.getId()));
	}

	

	@After
	public void finish() {
		this.george = null;
	}
}
